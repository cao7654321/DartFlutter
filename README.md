# DartFlutter

#### 介绍
在Flutter路上的点滴随笔：）

#### 背景

什么是Flutter？

Flutter 是一个开源UI框架，用于在Android和iOS上创建高级本机接口。Google最初发布的Flutter alpha发生在2017年5月.Flutter应用程序可以使用Dart编程语言编写。Flutter Beta版本于2月在2018年世界移动通信大会上宣布。

#### 软件架构
软件架构说明

Flutter有一个React风格的框架，其中包括随时可用的小部件，开发工具和渲染引擎。这些元素共同协作，可帮助您设计，构建和测试应用程序。

Flutter应用程序的基本构建组件是小部件。此外，在其他框架中，我们为视图，控制器和其他属性设置了不同的集合。每个小部件都有一个固定的UI声明。Flutter拥有统一的对象模型 - 小部件。

#### 功能

Flutter有自己的引擎，可以在Android和iOS上呈现应用程序以及UI组件。

Flutter使用Dart，这是一种快速，面向对象的语言，具有Minix，隔离，泛型和可选静态类型等功能。

Dart的另一个特殊方面是它可以使用Just-In-Time编译。

Flutter通过在开发过程中刷新来提供热重新加载，而无需全新的构建。

在Flutter中，我们可以使用IntelliJ IDEA， Android Studio或Visual Studio 开发应用程序。

它构建了一个小部件的想法。在Flutter中，您可以将小部件用于屏幕或应用程序本身。

使用Flutter，您可以通过用于动画，2D，效果，手势，渲染等的强大而灵活的API来解决复杂的UI挑战。

支持多个软件包，如Firebase实施，共享内容，打开图像，访问传感器等。

#### 开发环境选择与搭建


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 引用文章

https://baijiahao.baidu.com/s?id=1615906540318668603&wfr=spider&for=pc
#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
